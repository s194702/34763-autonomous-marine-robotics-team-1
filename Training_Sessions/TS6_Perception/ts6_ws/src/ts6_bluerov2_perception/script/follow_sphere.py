#!/usr/bin/env python

import cv2
import rospy
import math
from sensor_msgs.msg import Image, LaserScan
from std_msgs.msg import Float64MultiArray
import numpy as np
from cv_bridge import CvBridge
from statistics import median


class FollowSphere:
    def __init__(self):
        self.imu_sub = rospy.Subscriber(
            "/bluerov2/camera_front/camera_image/",
            Image,
            self.image_callback,
        )
        self.sonar_sub = rospy.Subscriber(
            "bluerov2/sonar_forward/", LaserScan, self.update_point_cloud
        )
        self.circle_img_pub = rospy.Publisher(
            "/bluerov2/circle_image/", Image, queue_size=1
        )
        self.gray_img_pub = rospy.Publisher(
            "/bluerov2/gray_image/", Image, queue_size=1
        )
        self.circle_response_pub = rospy.Publisher(
            "/bluerov2/circle_response/", Float64MultiArray, queue_size=1
        )

        rospy.init_node("follow_sphere", anonymous=False)
        self.point_cloud = []
        self.circles = []

    def convert_to_euclidian(self, r, a):
        if r < 10:
            x = r * math.cos(a)
            y = r * math.sin(a)
            self.point_cloud.append((x, y))

    def update_point_cloud(self, msg):
        self.point_cloud = []
        ranges = msg.ranges
        inc = msg.angle_increment
        initial_angle = msg.angle_min
        angles = [inc * i + initial_angle for i in range(len(ranges))]
        vdraw_point = np.vectorize(self.convert_to_euclidian)
        vdraw_point(ranges, angles)

    def plot_circles(self, img, circles, color):
        if circles is None:
            return
        for x, y, r in circles[0][0:1]:
            cv2.circle(img, (int(x), int(y)), int(r), color, 2)

    def draw_grid(self, img, width, height, color, radius=60):
        cv2.circle(img, (int(width / 2), int(height / 2)), radius, color, 2)
        cv2.line(
            img,
            (0, int(height / 2)),
            (int(width / 2) - radius, int(height / 2)),
            color,
            2,
        )
        cv2.line(
            img,
            (int(width / 2) + radius, int(height / 2)),
            (width, int(height / 2)),
            color,
            2,
        )
        cv2.line(
            img,
            (int(width / 2), 0),
            (int(width / 2), int(height / 2) - radius),
            color,
            2,
        )
        cv2.line(
            img,
            (int(width / 2), int(height / 2) + radius),
            (int(width / 2), int(height)),
            color,
            2,
        )

    def image_callback(self, image_msg):

        bridge = CvBridge()
        cv_image = bridge.imgmsg_to_cv2(image_msg, "bgr8")
        cv_image = cv2.resize(cv_image, (0, 0), fx=0.2, fy=0.2)
        gray = cv2.cvtColor(cv_image, cv2.COLOR_BGR2GRAY)
        height, width, col = cv_image.shape
        threshVal = 150
        ret, thresh = cv2.threshold(
            gray, threshVal, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU
        )
        circleRadius = (10, 200)
        print("Calculating..")
        circles = cv2.HoughCircles(
            thresh,
            cv2.HOUGH_GRADIENT,
            5,
            20,
            param2=70,
            minRadius=circleRadius[0],
            maxRadius=circleRadius[1],
        )

        if circles is not None:
            chosenCircle = circles[0][0]
            print(chosenCircle)
            x, y, radius = chosenCircle
            circle_height_visible_for_sonar = abs(y - height / 2) < radius - 5
            circle_close_enough_for_sonar = radius > 60
            circle_visible_for_sonar = (
                circle_height_visible_for_sonar and circle_close_enough_for_sonar
            )
            if circle_visible_for_sonar and len(self.point_cloud) == 0:
                self.circles.append((0, 0, 0))
                print("No legalcircle found!")
                self.plot_circles(cv_image, circles, (0, 0, 255))
            else:
                self.plot_circles(cv_image, circles, (0, 255, 0))
                self.circles.append((radius, x, y))
        else:
            self.circles.append((0, 0, 0))
            print("No circle found!")
        self.calculate_response(height, width)
        self.draw_grid(cv_image, width, height, (0, 255, 0))

        circle_img_msg = bridge.cv2_to_imgmsg(cv_image, "bgr8")
        self.circle_img_pub.publish(circle_img_msg)
        gray_tresh = thresh * 255
        gray_tresh_img_msg = bridge.cv2_to_imgmsg(gray_tresh, "mono8")
        self.gray_img_pub.publish(gray_tresh_img_msg)

    def calculate_response(self, height, width, toleranceRadius=100):
        if len(self.circles) >= 5:
            radius, x, y = median(self.circles[-5:])

            sway = 1 - 2 * x / width
            heave = 1 - 2 * y / height
            surge = max(1 - 2 * radius / toleranceRadius, -1)
            if radius == 0:
                surge = 0
                sway = 0
                heave = 0
            response_msg = Float64MultiArray()
            response_msg.data = [surge, sway, heave]
            self.circle_response_pub.publish(response_msg)

            return surge, sway, heave

    def run(self):
        rospy.spin()


if __name__ == "__main__":
    print("init follow sphere..")
    follow_sphere = FollowSphere()
    follow_sphere.run()
