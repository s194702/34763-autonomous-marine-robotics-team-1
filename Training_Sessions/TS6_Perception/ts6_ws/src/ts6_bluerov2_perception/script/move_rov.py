#!/usr/bin/env python
# Copyright (c) 2016 The UUV Simulator Authors.
# All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from __future__ import print_function
import os
import time
import sys, select, termios, tty
import rospy
import numpy as np
from std_msgs.msg import Bool
from geometry_msgs.msg import Twist, Accel, Vector3
from std_msgs.msg import Float64MultiArray


class MoveROV:

    def __init__(self):

        self.imu_sub = rospy.Subscriber(
            "/bluerov2/circle_response/",
            Float64MultiArray,
            self.response_callback,
        )

        self._output_pub = rospy.Publisher("/bluerov2/cmd_vel", Twist, queue_size=10)

        rospy.init_node("move_rov", anonymous=False)
        rate = rospy.Rate(10)  # 10 (Hz)

        self.speed_mul = 3

        self.cmd = Twist()
        self.cmd.linear = Vector3(0, 0, 0)
        self.cmd.angular = Vector3(0, 0, 0)

    def response_callback(self, response_msg):

        print("Received:")
        print(response_msg)

        def saturate_value(value):
            if abs(value) - 0.15 < 0:
                return 0
            return max(min(value, 1), -1)

        surge = saturate_value(response_msg.data[0]) * self.speed_mul
        sway = saturate_value(response_msg.data[1]) * self.speed_mul
        heave = saturate_value(response_msg.data[2]) * self.speed_mul

        # Probably need to change surge/sway/heave values here

        print("Applying the following surge/sway/heave twist:")
        print([surge, sway, heave])

        self.cmd = Twist()

        self.cmd.linear = Vector3(surge, sway, heave)

        self.cmd.angular = Vector3(0, 0, 0)

        print("sent:")
        print(self.cmd)

    def run(self):
        while not rospy.is_shutdown():
            self._output_pub.publish(self.cmd)


if __name__ == "__main__":
    print("init move_rov..")

    move_rov = MoveROV()
    move_rov.run()
    rospy.spin()
